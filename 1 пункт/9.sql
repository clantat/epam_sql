SELECT marks.subject
FROM marks
INNER JOIN student
ON student.student_id=marks.student_id
WHERE marks.mark = 2
GROUP BY marks.subject
HAVING count(marks.subject) > 2