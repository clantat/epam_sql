SELECT student.name
FROM marks
INNER JOIN student
ON student.student_id=marks.student_id
WHERE marks.mark = 2
GROUP BY student.name
HAVING count(marks.mark) = 2
