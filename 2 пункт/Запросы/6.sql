SELECT items.title, MAX(bids.bid_value)
FROM bids
INNER JOIN items
ON items.item_id=bids.items_item_id
GROUP BY items.title