SELECT user.full_name, AVG(bids.bid_value) AS "Ср.Цена"
FROM bids
INNER JOIN user
ON bids.user_id = user.user_id
GROUP BY user.full_name